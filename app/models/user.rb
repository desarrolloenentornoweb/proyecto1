class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
       enum role: [:user, :operator, :admin ]
       
       def admin_or_operator?
    self.admin? || self.operator?
  end
end
