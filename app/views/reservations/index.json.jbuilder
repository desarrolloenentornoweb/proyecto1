json.array!(@reservations) do |reservation|
  json.extract! reservation, :id, :reserved_at, :seats, :status, :waiting_allowed, :user_id
  json.url reservation_url(reservation, format: :json)
end
