# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160222024534) do

  create_table "contact1s", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "email",      limit: 255
    t.string   "phone",      limit: 255
    t.string   "message",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "email",      limit: 255
    t.string   "phone",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "credit_cards", force: :cascade do |t|
    t.string   "accountnumber", limit: 255
    t.string   "bankname",      limit: 255
    t.string   "cardnumber",    limit: 255
    t.date     "duedate"
    t.string   "cvv",           limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "favorite_places", force: :cascade do |t|
    t.string   "placename",  limit: 255
    t.string   "address",    limit: 255
    t.string   "district",   limit: 255
    t.integer  "closeto",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string   "comment",    limit: 255
    t.integer  "qualify",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "reservations", force: :cascade do |t|
    t.datetime "reserved_at"
    t.string   "direccion1",      limit: 255
    t.string   "direccion2",      limit: 255
    t.integer  "status",          limit: 4
    t.boolean  "waiting_allowed"
    t.integer  "user_id",         limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "reservations", ["user_id"], name: "index_reservations_on_user_id", using: :btree

  create_table "taxis", force: :cascade do |t|
    t.string   "tag",          limit: 255
    t.string   "manufacturer", limit: 255
    t.string   "model",        limit: 255
    t.integer  "seats",        limit: 4
    t.string   "color",        limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "",       null: false
    t.string   "encrypted_password",     limit: 255, default: "",       null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,        null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "role",                   limit: 255, default: "Seller"
    t.string   "name",                   limit: 255
    t.string   "last_name",              limit: 255
    t.string   "phone",                  limit: 255
    t.datetime "date_of_birth"
    t.boolean  "is_female",                          default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "reservations", "users"
end
