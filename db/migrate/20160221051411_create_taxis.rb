class CreateTaxis < ActiveRecord::Migration
  def change
    create_table :taxis do |t|
      t.string :tag
      t.string :manufacturer
      t.string :model
      t.integer :seats
      t.string :color

      t.timestamps null: false
    end
  end
end
