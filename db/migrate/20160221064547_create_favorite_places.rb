class CreateFavoritePlaces < ActiveRecord::Migration
  def change
    create_table :favorite_places do |t|
      t.string :placename
      t.string :address
      t.string :district
      t.integer :closeto

      t.timestamps null: false
    end
  end
end
