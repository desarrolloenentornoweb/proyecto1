class CreateCreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.string :accountnumber
      t.string :bankname
      t.string :cardnumber
      t.date :duedate
      t.string :cvv

      t.timestamps null: false
    end
  end
end
